var express = require('express');
var app = express();
const path = require('path')

app.use('/upload',express.static('upload'))

app.all('*', function (req, res, next) {
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin", "*");
    //允许的header类型
    res.header('Access-Control-Allow-Headers', 'Content-type');
    //跨域允许的请求方式
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS,PATCH");
    //可选，用来指定本次预检请求的有效期，单位为秒。在此期间，不用发出另一条预检请求。
    res.header('Access-Control-Max-Age', 1728000);//预请求缓存20天
    next();
});

var multer = require('multer');
var moment = require('moment');
var timestamp = '';
var timepath = moment().format('YYYY-MM-DD');
var destination = '/upload/' + timepath;

var storage = multer.diskStorage({
    //这里destination是一个字符串
    destination: '.' + destination,
    filename: function (req, file, cb){
        //自定义设置文件的名字
        timestamp = new Date().getTime();
        str = Math.random().toString(36).slice(-6)
        let  filename = 'upload-' + timestamp + '-'+str+'.' + file.originalname.split('.')[1];
        cb(null, filename)
    }
});

var upload = multer({
    storage: storage
});
    
//处理来自页面的ajax请求。单文件上传
app.post('/upload', upload.single('file'), function (req, res, next) {
    // console.log(req.file)
    //拼接文件上传后的路径
    var url = 'http://' + req.headers.host  + destination + '/' + req.file.filename;
    res.json({
        code: 1, 
        msg: '上传成功', 
        filePath: url
    })
});



var server = app.listen(10086, 'localhost', function () {

    var host = server.address().address
    var port = server.address().port

    console.log("服务启动成功，访问地址为 http://%s:%s", host, port)

})