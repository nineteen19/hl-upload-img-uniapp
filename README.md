# hl-uploadimg图片选择上传组件

## 介绍

图片上传前端组件，支持单图/多图，支持限定质量，支持同步/异步上传，可自定义图片、上传按钮、删除按钮样式。

### 兼容差异说明
支持H5，微信小程序，APP。


## 使用教程

### 1.导入插件
1.通过uniapp插件市场导入本插件到你的项目。[uniapp插件市场地址](https://ext.dcloud.net.cn/plugin?id=3629)
2.或者下载示例项目后将`components`目录下`hl-uploadimg`文件夹复制到你项目的`components`目录中,并将`static`目录下`hl-uploadimg`文件夹复制到你项目的`static`目录中。

**推荐直接通过uniapp插件市场导入**

### 2.在页面中添加组件引用
```js
<template>
    <view>
        <hl-uploadimg ref="hlUploadImg"></hl-uploadimg>
    </view>
</template>
```
本组件符合uniapp的`easycom`规范，`HBuilderX 2.5.5`起，只需在页面`template`中即可直接使用，无需在页面中`import`和注册`componets`

### 2.修改上传地址与响应字段
#####（1）打开组件目录下  `hl-uploadimg.js`文件，修改第一行上传地址为你自己的上传地址。
```js
const uploadPath = "http://127.0.0.1:10086/upload"; //修改此处的地址为你自己的上传地址
```

#####（2）修改第124行为你图片上传成功后响应的URL的字段名
```js
let _uploadedUrl = JSON.parse(uploadFileRes.data).filePath;          //按照你的实际响应修改
```
---
如果你的响应类似如下：
```JSON
{
    "code":0,
    "msg":"sucess",
    "data":{
        "url":"http://www.xxx123.com/20201119/bdf893b4255b4c7d8998b67c76f11535.png"
    }
}
```
那么122行应该修改为如下所示：
```js
let _uploadedUrl=uploadFileRes.data.url;   
```
---
如果你的响应类似如下：
```JSON
{
    "file":"http://www.xxx123.com/20201119/bdf893b4255b4c7d8998b67c76f11535.png"
}
```
那么122行应该修改为如下所示：

```js
let _uploadedUrl=uploadFileRes.file;   
```

**注意：不管怎么改，都应该确保最终赋给_uploadedUrl的值为你上传成功后的URL**

经过上面的修改，组建就可以正常上传图片和拿到上传后的地址了。

### 3.调用上传事件/监听上传回调
如果未开启自动上传，则需要手动触发上传事件
```js
// 上传图片
this.$refs.hlUploadImg.uploadImg().then(res=>{
    // doSomething...
    this.allUploadedImg=res.allImages;        //所有图片url数组
    this.thisUploadedImg=res.thisUploadedImages      //本次上传图片的url数组
});             
```
如果已开启自动上传，先要在组件上添加回调事件监听：
```js
<hl-uploadimg ref="hlUploadImg" @uploaded="uploadRes"></hl-uploadimg>
```
然后在页面的`methods`中监听回调
```js
// ...
methods:{
    uploadRes(res){
        // doSomething...
        this.allUploadedImg=res.allImages;        //所有图片url数组（可以用于提表单提交）
        this.thisUploadedImg=res.thisUploadedImages      //本次上传url数组
    }
}
// ...
```

## 属性、回调、事件

### 1.  属性说明
参数|类型|默认值|有效值|说明
--|--|--|--|--
model|String|upload|`upload`上传，默认值。<br/> `show` 展示，不显示选择和删除按钮，但需要传入`imgList`属性用于显示。|即使单张图片`imgList`也必须为数组
maxCount|Number|9|正整数|最多上传多少张。1为单图，大于1为多图上传。
overSize|Number|0|自然数|图片质量大小限制，单位为B。0不限制。建议写法：500KB写为`1024*500`，5MB可以写为`1024*1024*5`
columNum|Number|3|2~10都可以，但一般建议最大5|列数，每行多少张图片，默认3张。单图模式时失效该属性失效。
spaceBetween|Number|10|自然数|多图模式时，图片之间的间隔，单位像素`px`;默认`10px`，单图模式时失效
autoUpload|Boolean|false| 布尔值| 是否开启自动上传功能。如果开启自动上传功能，则在选择图片后会自动上传，否则要手动触发上传操作。
isAsync|Boolean|false|布尔值|是否为同步上传。同步上传，必须等到前一张图片上传成功才会上传后一张。异步上传，待上传图片一起上传
tapModel|String|''|'' 不做任何操作<br/>`preview` 浏览图片<br/>`replace` 替换图片|点击图片触发操作,展示模式时默认为`preview`
uploadingMask|String|uniLoading| `uniLoading` uniapp的loading<br/>`imgMask` 图片遮罩<br/>`dialogList` 模态框列表
imgMode|String|aspectFill|图片显示时剪裁、缩放模式|详见uniapp的image mode文档：[uniapp image组件](https://uniapp.dcloud.io/component/image) 
imgList|Array|[]|字符串数组|传入的图片。如果是修改情况下，我们可以传入一个图片url数组，用于显示原来的图片。
imgStyle|Object|null| |图片自定义样式，可以设置图片的圆角、高度等。
selectBtn|Object| | |选择图片按钮，见下方selectBtn属性说明
delBtn|Object| | |删除图片按钮，见下方delBtn属性说明

#### selectBtn属性说明
参数|类型|默认值|有效值|说明
--|--|--|--|--
text|String|上传图片| |按钮文字，传入空字符串可以不显示文本
icon|String|icon-add| |按钮图标(或图片)，传入空字符串可以不显示图标(或图片)
textSty|Object|null| |按钮文本样式
iconSty|Object|null| |按钮图标（或图片）样式
btnSty|Object|null| |选择图片按钮样式

#### delBtn属性说明
参数|类型|默认值|有效值|说明
--|--|--|--|--
icon|String|icon-roundclosefill| |删除按钮图标(或图片)
style|Object|`{top: '-20rpx'}`| |选择图片按钮自定义样式

### 2.回调方法  
方法名|回调类型|参数|说明
--|--|--|--
replaceImg|Object|`index` Number类型，被替换图片的索引<br/>`oldImage` String类型，旧图片url<br/>`newImage` String类型，新图片的url<br/> `uploadedImage` String类型，图片自动上传后的url（仅开启自动上传后有效）|替换图片回调方法
chooseImg|Object|`allImages`所有的图片<br/>`thisSelectImages`本次选择的图片<br/>`waitUploadImages`所有待上传的图片<br/>这个三参数都是Array类型|选择图片回调方法
uploaded|Object|`allImages`所有的图片<br/>`thisUploadedImages`本次上传的图片<br/>这两个参数都是Array类型|图片上传成功后的回调方法
delImg|Object| `allImages` 删除操作执行之后的所有图片 <br/> `delImage` 删除图片的URL

### 3.事件 
事件名|传参|类型|回调参数|说明
--|--|--|--|--
chooseImg()|无|Promise|`allImages`所有的图片<br/>`thisSelectImages`本次选择的图片<br/>`waitUploadImages`所有待上传的图片|选择图片
uploadImg()|无|Promise|`allImages`所有的图片<br/>`thisUploadedImages`本次上传的图片|上传图片

手动触发事件示例：
```js
// 选择图片
this.$refs.hlUploadImg.chooseImg().then(res=>{
    // doSomething...
    this.allList=res.allImages         //所有图片
    this.thisChooseImg=res.thisSelectImages      //本次选择的图片
    this.waitUploadImg=res.waitUploadImages      //所有待上传图片
});             

// 上传图片
this.$refs.hlUploadImg.uploadImg().then(res=>{
    // doSomething...
    this.allUploadedImg=res.allImages;		//所有图片
    this.thisUploadedImg=res.thisUploadedImages		//本次上传成功图片
});             
```
## 使用示例
### 1.传入图片
在实际开发中，通常修改数据的时候，会需要修改原来的图片，这时我们可以给组件传一个`imgList`,字符串数组，用于显示原来的图片。
代码示例：
```js
<template>
    <view class="upBox">
        <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList"></hl-uploadimg>
    </view>
</template>
<script>
export default {
    data() {
        return {
            oldImgList:[
                'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2583035764,1571388243&fm=26&gp=0.jpg',
                'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876535482,3811402221&fm=11&gp=0.jpg'
            ]
        };
    }
};
</script>

<style lang="scss"></style>
```
效果展示：

![demo1](https://test.hawklu.com/uploadimg/demo1.png)

**注意事项：**
单图模式如果传入图片，也必须为数组类型

### 2.自定义图片样式
默认图片是正方形，没有圆角，宽高为自动计算出来的。如果我们想要自定义样式，可以给组件传入`imgStyle`属性来设置自定义图片样式。


代码示例：
```js
<template>
    <view>
        <view class="upBox">
            <text class="rowTitle">默认图片样式，正方形，无圆角</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" ></hl-uploadimg>
            
            <text class="rowTitle">自定义样式1，图片高度为宽度的80%，10rpx的圆角</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :imgStyle="imgStyle"></hl-uploadimg>
            
            <text class="rowTitle">自定义样式2，宽高200rpx，圆形</text>
            <hl-uploadimg ref="hlUploadImg"  :imgList="oldImgList2" :imgStyle="imgStyle2"></hl-uploadimg>
        </view>
        
    </view>
</template>
<script>
export default {
    data() {
        return {
            oldImgList:[
                'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2583035764,1571388243&fm=26&gp=0.jpg',
                'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876535482,3811402221&fm=11&gp=0.jpg'
            ],
            oldImgList2:[
                'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876535482,3811402221&fm=11&gp=0.jpg'
            ],
            imgStyle:{
                height:0.8,
                borderRadius:'10rpx'
            },
            imgStyle2:{
                width:'200rpx',
                height:'200rpx',
                borderRadius:'50%',
                overflow: 'hidden'		//可以通过overFlow:hidden 隐藏删除按钮
            }
        };
    }
};
</script>

<style lang="scss">

</style>

```
效果展示：

![demo2](https://test.hawklu.com/uploadimg/demo2.png)

**注意事项：**
1. 图片默认为正方形，宽高为自动计算得出。
2. `height`高度设置,如果是按照高宽比设置，必须为Number数字类型，如上0.8，表示高度为宽度的80%
3. `height`高度设置,如果是直接设置rpx、px，必须为String字符串类型，例如`height:'200rpx'`
4. 如果设置图片样式，上传图片按钮会和图片样式保持一致，如果需要单独指定上传图片按钮样式，请设置组件属性`selectBtn`
5. 多图模式下不建议设置图片宽度，因为多图固定宽度后会导致样式不美观、换行异常。

### 3.自定义上传按钮样式
上传按钮默认的宽度、高度、圆角和图片样式保持一致，如果需要特殊指定，可以给组建传入`selectBtn`属性来设置。
```js
<template>
    <view>
        <view class="upBox">
            <text class="rowTitle">默认选择按钮样式</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" ></hl-uploadimg>
            
            <text class="rowTitle">选择按钮文本与样式</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :imgStyle="imgStyle" :selectBtn="upStyle"></hl-uploadimg>
            
            <text class="rowTitle">选择按钮图标与样式</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :imgStyle="imgStyle" :selectBtn="upStyle2"></hl-uploadimg>
            
            <text class="rowTitle">使用图片作为图标</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :imgStyle="imgStyle" :selectBtn="upStyle3"></hl-uploadimg>
        </view>
    </view>
</template>
<script>
    export default {
        data() {
            return {
                oldImgList: [
                    'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2583035764,1571388243&fm=26&gp=0.jpg',
                    'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876535482,3811402221&fm=11&gp=0.jpg'
                ],
                imgStyle: {
                    height: 0.8,
                    borderRadius: '10upx'
                },
                // 样式1
                upStyle: {
                    icon: '', //图标，空字符串不需要图标
                    text: '点击上传', //按钮文字
                    textSty:{			//文字样式
                        fontSize:'24rpx',
                        color:'#ffffff'
                    },
                    btnSty: {
                        border: 'none',
                        background: '#007aff',
                        borderRadius: '0px'
                    }
                },
                // 样式2
                upStyle2: {
                    icon: 'icon-camerafill', //图标，空字符串不需要图标
                    iconSty:{			//图标样式
                        color: "#67C23A",
                    },
                    text: '', //文本  空字符串不需要要文字
                    btnSty: {
                        border: '1px dashed #67C23A',
                        borderRadius: '0px'
                    }
                },
                // 样式3
                upStyle3:{
                    icon:'../../static/del.png',
                    iconSty:{
                        width:'66rpx',
                        height:'66rpx'
                    }
                }
                
            };
        }
    };
</script>
```
效果展示：

![demo3](https://test.hawklu.com/uploadimg/demo3.png)

**注意事项**
1. 多图上传模式，已选择图片达到最大上传张数时，上传按钮自动隐藏
2. 该组件使用的图标来自阿里图标库，如果需要自定义图标，可以自己修组件目录下`hlUploadImg.vue`文件中样式表第一行引用。目前该组件只配置了以下几种图标：
![demo4](https://test.hawklu.com/pic/demo4.png)
该组图标大部分来自iconfont官方图标库中的手淘图标库，地址：[iconfont](https://www.iconfont.cn/collections/detail?spm=a313x.7781069.1998910419.d9df05512&cid=33)
图标类名可见`static/hl-uploadimg`下`demo_index.html`文件

### 4. 自定义删除按钮
图片删除按钮默认为红色填充图标，如果需要自定义删除按钮，可以给组件传入`delBtn`属性
代码示例：
```js
<template>
    <view>
        <view class="upBox">
            <text class="rowTitle">默认删除按钮样式</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList"></hl-uploadimg>
            <text class="rowTitle">自定义删除按钮样式</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :delBtn="delStyle"></hl-uploadimg>
            
            <text class="rowTitle">使用图片作为删除按钮</text>
            <hl-uploadimg ref="hlUploadImg" :imgList="oldImgList" :delBtn="delStyle2"></hl-uploadimg>
        </view>
    </view>
</template>
<script>
    export default {
        data() {
            return {
                oldImgList: [
                    'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2583035764,1571388243&fm=26&gp=0.jpg',
                    'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876535482,3811402221&fm=11&gp=0.jpg'
                ],
                delStyle: {
                    icon: 'icon-close',
                    style: {
                        display: 'block',
                        width: '40upx',
                        height: '40upx',
                        textAlign: 'center',
                        color: '#ffffff',
                        background: '#000000',
                        lineHeight: '40upx',
                        right: '0',
                        top: '0'
                    }
                },
                delStyle2:{
                    icon: '../../static/del.png',
                    style: {
                        width: '40rpx',
                        height: '40rpx',
                        left:'0',
                        bottom:'0'
                    }
                }

            };
        }
    };
</script>
```
效果展示：

![demo5](https://test.hawklu.com/uploadimg/demo5.png)

**注意事项**
1. 删除按钮定位为`absolute`绝对定位，参照系为每张图片外部隐藏的容器，该容器大小和图片一致。
2. 可以使用`top`、`left`、`right`、`bottom`来控制删除按钮的位置


### 5.点击图片触发操作

当有图片时，默认点击图片不会触发任何操作，如果需要相关操作，可以设定组件的`tapModel`属性。目前有两种模式可供选择：
- `''` 空字符串，默认值，不做任何操作
- `preview` 浏览图片，使用的`uni.previewImage()`
- `replace` 替换被点击图片

**注意事项：**

如果开启自动上传，替换图片时会自动上传替换后的图片，并且通过`replaceImg`可以获取自动上传后的回调属性


### 6.上传时遮罩
图片上传一般需要花费一定的时间，所以在上传过程中应该给用户明确的提示信息。可以通过设定组件的`uploadingMask`属性来指定上传时的遮罩。
目前只有3重方式供选择

1.`uniLoading`uniapp提供的loading

![demo6](https://test.hawklu.com/pic/demo6.png)

2.`imgMask` 在每张图片上显示的遮罩

![demo7](https://test.hawklu.com/pic/demo7.png)

3.`dialogList`弹窗列表进度条

![demo8](https://test.hawklu.com/pic/demo8.png)

**注意事项**
1.dialogList模式下，显示上传成功时虽然显示所有图片，但该组件会自动过滤网络图片。

###  7.使用你自己的图标
如果你的项目中已有图标库，你可以修改组件`hlUploadImg.vue`文件底部`style`标签内的第一行的引用。使用你自己的图标。

## 常见问题
#### 1.所有样式对象的规范
所有的样式的书写，如果样式名带连字符“-”，请去掉连字符并将连字符后面的第一个字母大写。例如`text-align`应改写为`textAlign`，或使用单引号/双引号包裹样式名，例如：`font-size:32rpx;`应该改写为`'font-size':'32rpx'`

#### 2.小程序不支持`upx`单位
由于小程序不支持行内样式使用Object,所以在小程序端，会将所有的样式对象转换为字符串。因此小程序不支持`upx`单位。APP和H5支持`upx`。

#### 3.图片上传不成功
1. H5请排查你的上传接口是否支持跨域，如果不支持，请自行搜索处理。
2. 小程序端需要将你的上传接口域名添加到uploadFile合法域名中。

#### 4.示例项目无法运行
1.示例项目使用了`scss`，请自行安装`scss`编译组件
2.小程序请将小小程序的APPID更换为你自己的APPID
3.uniapp直接运行项目，重新获取uni-app应用标识

## 更新计划
- [ ]拖拽功能（正在开发中）
- [ ]图片压缩功能
- [ ]图片与base64互相转换

## bug反馈与交流

bug反馈企鹅群：924907604

作者企鹅：185328602 验证请填写 hl-uploadimg

Gitee：[码云Gitee](https://gitee.com/nineteen19/hl-upload-img-uniapp)

**开源不易，如果好用，请给Star**